#include <iostream>
#include <zmqpp/zmqpp.hpp>
#include <fstream>
#define MAX 20

using namespace std;

int main(int argc, char *argv[]) {
  const string endpoint = "tcp://localhost:4242";
  if(argc !=3 && argc!=4){
      cerr << "Error calling the program" << endl;
      return 1;
  }
  zmqpp::context context;
  zmqpp::socket_type type = zmqpp::socket_type::request;
  zmqpp::socket socket (context, type);
  cout << "Opening connection to " << endpoint << "..." << endl;
  socket.connect(endpoint);

  cout << "Sending text and a number..." << endl;
  zmqpp::message req;

  if(strcmp(argv[1],"Mult")==0){
      if(argc !=4){
          cerr << "Error calling the program" << endl;
          return 1;
      }
      ifstream mtz1(argv[2]);
      ifstream mtz2(argv[3]);
      //pidiendo los datos.
      int fila1=0,columna1=0;
      int fila2=0,columna2=0;
      //datos de la primera matriz
      mtz1 >> fila1;
      mtz1 >> columna1;
      //datos de la segunda matriz
      mtz2 >> fila2;
      mtz2 >> columna2;
      req << argv[1];
      req << fila1 << columna1 << fila2 << columna2;
      float m1[fila1][columna1];
      float matriz_lineal;
      for(int i=0;i<fila1;i++){
          for(int j=0;j<columna1;j++){
              mtz1 >> matriz_lineal;
              req << matriz_lineal;
          }
      }
      float m2[fila2][columna2];
      for(int i=0;i<fila2;i++){
          for(int j=0;j<columna2;j++){
              mtz2 >> matriz_lineal;
              req << matriz_lineal;
          }
      }
  }else if (strcmp(argv[1],"Tras")==0){
      if(argc !=3){
          cerr << "Error calling the program" << endl;
          return 1;
      }
      ifstream mtz1(argv[2]);
      int fila=0;
      int columna=0;
      mtz1 >> fila;
      mtz1 >> columna;
      req << argv[1] << fila << columna;
      float m1[fila][columna];
      float matriz_lineal;
      for(int i=0;i<fila;i++){
          for(int j=0;j<columna;j++){
              mtz1 >> matriz_lineal;
              req << matriz_lineal;
          }
      }
  }
  socket.send(req);
  cout << "Request Sent message." << endl;
  zmqpp::message rep;//respuesta
  socket.receive(rep);
  int codigo_recepcion=0;
  rep >> codigo_recepcion;
  if (codigo_recepcion==1){
      int fila=0;
      int columna=0;
      rep >> fila >> columna;
      float matriz[MAX][MAX];
      for (int i=0;i<fila;i++){
          for(int j=0;j<columna;j++){
              rep >> matriz[i][j];
              cout << matriz[i][j] << "\t";
          }
          cout << endl;
      }
  }else if(codigo_recepcion==-1){
      string mensaje_error;
      rep >> mensaje_error;
      cout << mensaje_error << endl;
  }


  cout << "Finished." << endl;
}
