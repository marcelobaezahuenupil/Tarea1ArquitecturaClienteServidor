#include <zmqpp/zmqpp.hpp>
#include <string>
#include <iostream>
#include <math.h>
#include <algorithm>
#define MAX 20

using namespace std;

float cofactor (float matriz[][MAX], int tam, int fila, int columna);
float determinante (float matriz[][MAX], int tam);
void Inversa(float m1[][MAX], float identidad[][MAX],int fila,int columna);
int main(int argc, char *argv[]) {
  const string endpoint = "tcp://*:4242";
  // initialize the 0MQ context
  zmqpp::context context;

  // generate a pull socket
  zmqpp::socket_type type = zmqpp::socket_type::reply;
  zmqpp::socket socket (context, type);

  // bind to the socket
  cout << "Binding to " << endpoint << "..." << endl;
  socket.bind(endpoint);
  zmqpp::message mensaje;
  socket.receive(mensaje);

  //al recibir..
  string operacion;
  mensaje >> operacion;
  //Para responder
  zmqpp::message resp;

  if(operacion=="Tras"){
      int fila=0;
      int columna=0;
      mensaje >> fila;
      mensaje >> columna;
      if(fila==columna){
          float m1[MAX][MAX];
          for(int i=0;i<fila;i++){
              for(int j=0;j<columna;j++){
                  mensaje >> m1[i][j];
              }
          }
          //Comprobar determinante;
          float determinate_m;
          determinate_m = determinante(m1 , fila);
          if(determinate_m!=0.0){// Obtener la inversa
              float identidad[MAX][MAX];
              for(int i=0;i<fila;i++){
                  for (int j=0;j<fila;j++){
                      if(i==j)identidad[i][j]=1;
                      else identidad[i][j]=0;
                  }
              }
              Inversa(m1,identidad,fila,columna);
              resp << 1;
              resp << fila << fila;
              for(int i=0;i<fila;i++){
                  for(int j=0;j<fila;j++){
                      resp << identidad[i][j];
                      cout << identidad[i][j] << "\t";
                  }
                  cout << endl;
              }

          }else{
              string respuesta_error;
              respuesta_error="Estimado, la matriz no posee inversa." ;
              resp << -1;
              resp << respuesta_error;
          }

      }else{
          string respuesta_error;
          respuesta_error="Estimado, para calcular el determionante de una matriz, esta debe ser cuadrada" ;
          resp << -1;
          resp << respuesta_error;
      }
  }else if (operacion=="Mult"){
      int fila1=0,columna1=0;
      int fila2=0,columna2=0;
      mensaje >> fila1 >> columna1 >> fila2 >> columna2;
      float m1[fila1][columna1];
      for(int i=0;i<fila1;i++){
          for(int j=0;j<columna1;j++){
              mensaje >> m1[i][j];
          }
      }
      float m2[fila2][columna2];
      for(int i=0;i<fila2;i++){
          for(int j=0;j<columna2;j++){
              mensaje >> m2[i][j];
          }
      }
      //Multiplicacion
      if(fila1 == columna1 && fila1==fila2 && fila1==columna2 && columna1==fila2 && columna1==columna2){
          int fila1=0,columna1=0;
          int fila2=0,columna2=0;
          mensaje >> fila1 << columna1 << fila2 << columna2;
          float resul[fila1][columna2];
          for(int i=0;i<fila1;i++){
              for(int j=0;j<columna2;j++){
                  int valor=0;
                  for(int a=0;a<fila1;a++){
                      valor+=m1[i][a]*m2[a][j];
                  }
                  resul[i][j]=valor;
              }
          }
          resp << 1;
          resp << fila1 << fila1;
          for(int i=0;i<fila1;i++){
              for(int j=0;j<fila1;j++){
                  resp << resul[i][j];
              }
          }
      }if(columna1==fila2){// codición para matrices de diferentes tamaños..
          float resul[fila1][columna2];
          for(int i=0;i<fila1;i++){
              for(int j=0;j<columna2;j++){
                  int valor=0;
                  for(int a=0;a<columna1;a++){
                      valor+=m1[i][a]*m2[a][j];
                  }
                  resul[i][j]=valor;
              }
          }
          resp << 1;
          resp << fila1 << columna2;
          for(int i=0;i<fila1;i++){
              for(int j=0;j<columna2;j++){
                  resp << resul[i][j];
                  cout << resul[i][j] << "\t";
              }
              cout << endl;
          }
      }else {
          string mensaje_error;
          mensaje_error="No se pueden multiplicar las matrices, porque el numero de columna de la primera matriz es disinto al numero de filas de la segunda matriz";
          resp << -1;
          resp << mensaje_error;
      }
  }
  socket.send(resp);
  cout << "Finished." << endl;
}
float determinante (float matriz[][MAX], int tam){
    float det=0.0;
    if(tam==1){
        det=matriz[0][0];
    }else{
        for(int i=0;i<tam;i++){
            det=det + matriz[0][i] * cofactor(matriz,tam,0,i);
        }
    }
    return det;
}
float cofactor (float matriz[][MAX], int tam, int fila, int columna){
    float submatriz[MAX][MAX];
    int n=tam-1;
    int x=0;
    int y=0;
    for(int i=0;i<tam;i++){
        for(int j=0;j<tam;j++){
            if(i!=fila && j!=columna){
                submatriz[x][y]=matriz[i][j];
                y++;
                if(y>=n){
                    x++;
                    y=0;
                }
            }
        }
    }
    return pow(-1.0, fila+columna) * determinante(submatriz,n);
}
void Inversa(float m1[][MAX], float identidad[][MAX],int fila,int columna){
    for (int i=0;i<fila;i++){
        volver:
            for (int j=i;j<fila;j++){
                float valor1=m1[i][j];
                float valor2=m1[j][i];
                if(m1[i][i]==0){
                    //cambios para la original
                    float arr1[fila];
                    float arr2[fila];
                    //cambios para la de identidad
                    float ide1[fila];
                    float ide2[fila];
                    int posicioncambio=-1;
                    for(int a1=0;a1<fila;a1++){
                        if(m1[a1][j]!=0){
                            posicioncambio=a1;
                            break;
                        }
                    }
                    for(int algo=0;algo<fila;algo++){
                        //Guardar fila, para cambiar posicion de la matriz original
                        arr1[algo]=m1[i][algo];
                        arr2[algo]=m1[posicioncambio][algo];
                        //Guardar fila, para cambiar posicion de la matriz identidad
                        ide1[algo]=identidad[i][algo];
                        ide2[algo]=identidad[posicioncambio][algo];

                    }
                    for(int a3=0;a3<fila;a3++){
                        //hacer cambios en la matriz original
                        m1[posicioncambio][a3]=arr1[a3];
                        m1[i][a3]=arr2[a3];
                        //Hacer cambios en la matriz identidad
                        identidad[posicioncambio][a3]=ide1[a3];
                        identidad[i][a3]=ide2[a3];
                    }
                    goto volver;
                }
                if(valor2==0){
                    continue;
                }else if (m1[i][i]==1){
                    int posicion=-1;
                    for(int a4=0;a4<fila;a4++){
                        if(m1[a4][i]!=0 && a4!=i){
                            posicion=a4;
                            break;
                        }
                    }
                    if(posicion==-1)break;
                    float valor3=m1[i][i];
                    float valor4=m1[posicion][i];
                    for (int A=0;A<fila;A++){
                        m1[posicion][A]=valor4*m1[i][A] - valor3*m1[posicion][A];
                        identidad[posicion][A]=valor4*identidad[i][A] - valor3*identidad[posicion][A];
                    }
                }
                if(m1[i][i]!=1){
                    float algo=m1[i][i];
                    for(int a2=0;a2<fila;a2++){
                        if(m1[i][a2]==0 && identidad[i][a2]==0)continue;
                        m1[i][a2]=(m1[i][a2]/algo);
                        identidad[i][a2]=(identidad[i][a2]/algo);
                    }
                    goto volver;
                }
            }
    }
    for(int i=(fila-1);i>0;i--){
        for(int j=(fila-1);j>0;j--){
            if(m1[j][i]!=0 && i!=j){
                float valor1= m1[i][i];
                float valor2= m1[j][i];
                for(int a1=0;a1<fila;a1++){
                    m1[j][a1]=valor1*m1[j][a1] - valor2*m1[i][a1];
                    identidad[j][a1]=valor1*identidad[j][a1] - valor2*identidad[i][a1];
                }
            }
        }
    }

}
